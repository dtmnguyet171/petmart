package com.vti.petmart.controller;

import com.vti.petmart.modal.dto.AccountCreateRequest;
import com.vti.petmart.modal.entity.Account;
import com.vti.petmart.service.iml.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/account")
@CrossOrigin("*")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping("/get-all")
    public List<Account> getAll() {
        return accountService.getAll();
    }

    @PostMapping("/create")
    public Account create(@RequestBody AccountCreateRequest request){
        return accountService.create(request);
    }
}
