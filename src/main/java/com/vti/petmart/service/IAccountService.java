package com.vti.petmart.service;

import com.vti.petmart.modal.dto.AccountCreateRequest;
import com.vti.petmart.modal.entity.Account;

import java.util.List;

public interface IAccountService {
    List<Account> getAll();

    Account create(AccountCreateRequest request);
}
