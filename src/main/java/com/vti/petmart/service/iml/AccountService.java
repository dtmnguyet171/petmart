package com.vti.petmart.service.iml;

import com.vti.petmart.modal.dto.AccountCreateRequest;
import com.vti.petmart.modal.entity.Account;
import com.vti.petmart.modal.entity.Role;
import com.vti.petmart.repository.IAccountRepository;
import com.vti.petmart.service.IAccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService implements IAccountService {
    @Autowired
    private IAccountRepository accountRepository;

    @Override
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account create(AccountCreateRequest request){
        Account account = new Account();
        BeanUtils.copyProperties(request, account);
        account.setRole(Role.USER);
        account = accountRepository.save(account);
        return account;
    }

}
