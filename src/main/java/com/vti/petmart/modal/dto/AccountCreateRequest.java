package com.vti.petmart.modal.dto;

import com.vti.petmart.modal.entity.Role;
import lombok.Data;

import javax.persistence.Column;
@Data
public class AccountCreateRequest {
    private String username;

    private String fullName;

    private String email;

    private String password;

    private String avatar;
}
