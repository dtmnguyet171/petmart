package com.vti.petmart.modal.entity;

public enum Status {
    PENDING, DONE, CANCEL
}
