package com.vti.petmart.modal.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "`PRODUCT`")
public class Product {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "price",nullable = false)
    private int price;

    @Column(name = "image")
    private String image;

    @Column(name = "category")
    private Category category;

    @Column(name = "desciption")
    private String description;
}
