package com.vti.petmart.modal.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "`ACTIVE_USER`")
public class ActiveUser {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "uuid", unique = true)
    private String uuid;

    @OneToOne
    @JoinColumn(name = "user_id")
    private Account account;
}
