package com.vti.petmart.modal.entity;

public enum Category {
    FOOD, HYGIENE, ACCESSORIES, MEDICINE
}
